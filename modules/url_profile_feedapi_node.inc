<?php
/* $Id: */

/**
 * URL profile implementation in behalf of feedapi_node_item.
 */

/**
 * Implementation of hook_url_profile().
 */
function feedapi_node_url_profile(&$object, $op, $arg = NULL) {
  
  switch ($op) {
    case 'list':
      $list = array();
      // sometimes stuff comes in as array - todo: clean this up
      if ($url_str = db_result(db_query('SELECT url FROM {feedapi_node_item} WHERE nid = %d', $object->nid))) {
        $list[] = $url_str;
      }
      return $list;
    case 'list_matches':
    /*
      if (!is_object($object)) {
        drupal_set_message('No object!', 'error');
      }*/
      $result = db_query ('SELECT url
                           FROM {feedapi_node_item}
                           WHERE url LIKE "%s%%"
                           AND url LIKE "%%%s%%" ',
                           $object->url, $object->criteria);
      while ($feed_item = db_fetch_object($result)) {
        $list[] = $feed_item;
      }
      return $list;
  }
}